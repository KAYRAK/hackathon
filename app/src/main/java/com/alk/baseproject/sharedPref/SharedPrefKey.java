package com.alk.baseproject.sharedPref;

public enum SharedPrefKey {
    EXAMPLE_KEY("EXAMPLE_KEY"),
    CLIENT_ID("CLIENT_ID");

    private final String text;

    SharedPrefKey(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}

package com.alk.baseproject.sharedPref;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public enum SharedPrefManager {
    INSTANCE;

    private static SharedPreferences pref;

    public String getString(SharedPrefKey key) {
        return pref.getString(key.toString(), null);
    }

    public void setString(SharedPrefKey key, String value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key.toString(), value);
        editor.commit();
    }

    public void setStringAsync(SharedPrefKey key, String value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key.toString(), value);
        editor.apply();
    }

    @Deprecated
    public void setString(String key, String value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    @Deprecated
    public String getString(String key) {
        return pref.getString(key, null);
    }

    public float getFloat(SharedPrefKey key, float defaultValue) {
        return pref.getFloat(key.toString(), defaultValue);
    }

    public void setFloat(SharedPrefKey key, float value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(key.toString(), value);
        editor.commit();
    }

    public long getLong(SharedPrefKey key, long defaultValue) {
        return pref.getLong(key.toString(), defaultValue);
    }

    public void setLong(SharedPrefKey key, long value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(key.toString(), value);
        editor.commit();

    }

    public int getInteger(SharedPrefKey key, int defaultValue) {
        return pref.getInt(key.toString(), defaultValue);
    }

    public void setInteger(SharedPrefKey key, int value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key.toString(), value);
        editor.commit();
    }

    public boolean getBoolean(SharedPrefKey key, boolean defaultValue) {
        return pref.getBoolean(key.toString(), defaultValue);
    }

    @Deprecated
    public boolean getBoolean(String key, boolean defaultValue) {
        return pref.getBoolean(key, defaultValue);
    }

    public void setBoolean(SharedPrefKey key, boolean value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key.toString(), value);
        editor.commit();
    }

    @Deprecated
    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void remove(SharedPrefKey key) {
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(key.toString());
        editor.apply();
    }

    @Deprecated
    public void remove(String key) {
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(key);
        editor.apply();
    }

    public void init(Context context) {
        if (pref != null)
            throw new RuntimeException();
        pref = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }
}

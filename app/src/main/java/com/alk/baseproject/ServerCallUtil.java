package com.alk.baseproject;

import android.util.Log;

import com.alk.baseproject.models.PushTokenRequestModel;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by akayrak on 24/08/2017.
 */

public class ServerCallUtil {

    public static void sendPushTokenToServer() {

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        PushTokenRequestModel pushTokenRequestModel = new PushTokenRequestModel();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        if(refreshedToken == null) {
            return;
        }

        pushTokenRequestModel.setDeviceToken(refreshedToken);

        RequestBody body = RequestBody.create(JSON, new Gson().toJson(pushTokenRequestModel, PushTokenRequestModel.class));

        Request request = new Request.Builder()
                .url("http://172.20.10.3:8080/push")
                .post(body)
                .build();


        AppController.getInstance().getOkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("TAG", e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {
                    return;
                }

                String result = response.body().string();

                //Collection<Profile> results = new Gson().fromJson(result, token);

            }
        });

    }
}

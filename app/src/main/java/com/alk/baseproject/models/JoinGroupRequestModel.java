package com.alk.baseproject.models;

/**
 * Created by akayrak on 03/09/2017.
 */

public class JoinGroupRequestModel extends CreateGroupRequestModel {
    private String groupCode;

    public JoinGroupRequestModel() {
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }
}

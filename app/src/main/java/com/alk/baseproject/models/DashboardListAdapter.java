package com.alk.baseproject.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.alk.baseproject.Constants;
import com.alk.baseproject.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by akayrak on 03/09/2017.
 */

public class DashboardListAdapter extends ArrayAdapter<AccountDataModel> implements View.OnClickListener {

    Context mContext;
    private ArrayList<AccountDataModel> dataSet;

    public DashboardListAdapter(ArrayList<AccountDataModel> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        AccountDataModel AccountDataModel = (AccountDataModel) object;

        /*
        switch (v.getId())
        {
            case R.id.item_info:
                Snackbar.make(v, "Release date " +AccountDataModel.getFeature(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
        }
        */
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        AccountDataModel accountDataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtBalance = (TextView) convertView.findViewById(R.id.txtBalance);
            viewHolder.sqr = (View) convertView.findViewById(R.id.sqr);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtName.setText(accountDataModel.getName());

        viewHolder.txtBalance.setText(NumberFormat.getCurrencyInstance(new Locale("it", "IT"))
                .format(accountDataModel.getBalance()));


        viewHolder.sqr.setBackgroundColor(Constants.colors.get((accountDataModel.getColorCode() % Constants.colors.size())));

        //viewHolder.txtName.setTextColor(Constants.colors.get((accountDataModel.getColorCode() % Constants.colors.size())));

        return convertView;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtBalance;
        View sqr;
    }
}

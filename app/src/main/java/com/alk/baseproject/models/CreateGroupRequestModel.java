package com.alk.baseproject.models;

import com.alk.baseproject.models.server.Account;

import java.util.ArrayList;

/**
 * Created by akayrak on 03/09/2017.
 */

public class CreateGroupRequestModel {
    private String facebookId;
    private String groupName;

    private ArrayList<Account> accounts;

    public CreateGroupRequestModel(String facebookId, ArrayList<Account> accounts) {
        this.facebookId = facebookId;
        this.accounts = accounts;
    }

    public CreateGroupRequestModel() {
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}

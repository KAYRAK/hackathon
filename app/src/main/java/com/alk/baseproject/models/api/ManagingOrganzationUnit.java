
package com.alk.baseproject.models.api;


public class ManagingOrganzationUnit {

    private String name;
    private String organizationBIC;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizationBIC() {
        return organizationBIC;
    }

    public void setOrganizationBIC(String organizationBIC) {
        this.organizationBIC = organizationBIC;
    }

}

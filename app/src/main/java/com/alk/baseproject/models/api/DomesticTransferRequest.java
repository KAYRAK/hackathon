package com.alk.baseproject.models.api;

/**
 * Created by akayrak on 02/09/2017.
 */

public class DomesticTransferRequest {

    private Integer amount;
    private String currency;
    private String cause;
    private Integer requestedExecutionDate;
    private Recipient recipient;
    private Sender sender;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public Integer getRequestedExecutionDate() {
        return requestedExecutionDate;
    }

    public void setRequestedExecutionDate(Integer requestedExecutionDate) {
        this.requestedExecutionDate = requestedExecutionDate;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

}

package com.alk.baseproject.models;

/**
 * Created by akayrak on 24/08/2017.
 */

public class PushTokenRequestModel {
    private String deviceToken;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}

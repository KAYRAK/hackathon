package com.alk.baseproject.models.server;

/**
 * Created by akayrak on 03/09/2017.
 */

public class Account {
    private String bankid;
    private String accountid;

    public Account() {
    }

    public Account(String bankid, String accountid) {
        this.bankid = bankid;
        this.accountid = accountid;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }
}

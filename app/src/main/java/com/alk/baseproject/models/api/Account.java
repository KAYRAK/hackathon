
package com.alk.baseproject.models.api;


public class Account {

    private String id;
    private String name;
    private String accountType;
    private String currency;
    private ArrangementIdentifier arrangementIdentifier;
    private String holder;
    private ManagingOrganzationUnit managingOrganzationUnit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ArrangementIdentifier getArrangementIdentifier() {
        return arrangementIdentifier;
    }

    public void setArrangementIdentifier(ArrangementIdentifier arrangementIdentifier) {
        this.arrangementIdentifier = arrangementIdentifier;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public ManagingOrganzationUnit getManagingOrganzationUnit() {
        return managingOrganzationUnit;
    }

    public void setManagingOrganzationUnit(ManagingOrganzationUnit managingOrganzationUnit) {
        this.managingOrganzationUnit = managingOrganzationUnit;
    }

}

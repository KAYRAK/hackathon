package com.alk.baseproject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import okhttp3.Request;

/**
 * Created by akayrak on 17/08/2017.
 */

public class Util {


    public static Request createRequest(String url) {

        Request request = new Request.Builder()
                .url("https://api.unicredit.eu/" + url)
                .addHeader("keyId", "cfe46594-b9a6-49f1-97d1-63de291fa9fe")
                .build();

        return request;
    }

    public static Request.Builder createPostRequest(String url) {

        Request.Builder request = new Request.Builder()
                .url("https://api.unicredit.eu/" + url)
                .addHeader("keyId", "cfe46594-b9a6-49f1-97d1-63de291fa9fe")
                ;

        return request;
    }


    public void restart(Context context, int delay) {
        if (delay == 0) {
            delay = 1;
        }

        Intent restartIntent = context.getPackageManager()
                .getLaunchIntentForPackage(context.getPackageName());
        PendingIntent intent = PendingIntent.getActivity(
                context, 0,
                restartIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
        System.exit(2);
    }
}

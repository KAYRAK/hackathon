package com.alk.baseproject.service;

import android.annotation.SuppressLint;
import android.util.Log;

import com.alk.baseproject.ServerCallUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.greenrobot.eventbus.EventBus;

@SuppressWarnings("HardCodedStringLiteral")
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseInstanceIDService";


    @SuppressLint("LongLogTag")
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        //android.os.Debug.waitForDebugger();
        sendRegistrationToServer(refreshedToken);

    }


    private void sendRegistrationToServer(String token) {


        ServerCallUtil.sendPushTokenToServer();

        //EventBus.getDefault().post(new ChatService.ChatServiceCommunicationEvent(ChatService.ChatServiceCommunicationEvent.Comm_Type.SAVE_TOKEN_DB).setToken(token));

    }
}

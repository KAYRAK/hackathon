package com.alk.baseproject.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.alk.baseproject.AppController;
import com.alk.baseproject.unuseds.SampleActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import br.com.goncalves.pugnotification.notification.Load;
import br.com.goncalves.pugnotification.notification.PugNotification;

@SuppressWarnings("HardCodedStringLiteral")
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessagingService";

    @SuppressLint("LongLogTag")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        Intent notificationIntent = new Intent(getApplicationContext(), SampleActivity.class);

        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Load load = PugNotification.with(AppController.getInstance().getApplicationContext())
                .load()
                .title("messageTitle")
                .message("messageBody")
                .largeIcon(android.R.drawable.ic_dialog_info)
                .smallIcon(android.R.drawable.ic_dialog_info)
                .flags(Notification.DEFAULT_ALL)
                .autoCancel(true);

        load.click(intent);

        load.simple().build();


    }

}

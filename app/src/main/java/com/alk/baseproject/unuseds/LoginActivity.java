package com.alk.baseproject.unuseds;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.alk.baseproject.R;
import com.alk.baseproject.ServerCallUtil;
import com.alk.baseproject.base.BaseActivity;
import com.alk.baseproject.models.CreateUserRequestModel;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends BaseActivity {

    private CallbackManager callbackManager;

    private LoginButton fbLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        callbackManager = CallbackManager.Factory.create();
        fbLoginButton = (LoginButton) findViewById(R.id.fb_login);
        fbLoginButton.setReadPermissions("user_friends");

        redirect();

        // Callback registration
        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                // App code
                Toast.makeText(
                        LoginActivity.this,
                        R.string.success,
                        Toast.LENGTH_SHORT).show();

                Set declinedPermissions = AccessToken.getCurrentAccessToken().getDeclinedPermissions();

                if (declinedPermissions != null) {
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, declinedPermissions);
                } else {
                    redirect();
                }
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(
                        LoginActivity.this,
                        R.string.cancel,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(final FacebookException exception) {
                // App code
                Toast.makeText(
                        LoginActivity.this,
                        R.string.error,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void redirect() {

        if (isLoggedIn()) {


            String refreshedToken = FirebaseInstanceId.getInstance().getToken();

            if(refreshedToken != null) {
                ServerCallUtil.sendPushTokenToServer();
                return;
            }

            //new LoadingDialog(this).show();

            AccessToken accesstoken = AccessToken.getCurrentAccessToken();

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            CreateUserRequestModel createUserRequest = new CreateUserRequestModel();
            createUserRequest.setAccessToken(accesstoken.getToken());
            createUserRequest.setProviderId("FACEBOOK");
            createUserRequest.setUserId(Profile.getCurrentProfile().getId());

            RequestBody body = RequestBody.create(JSON, new Gson().toJson(createUserRequest, CreateUserRequestModel.class));

            Request request = new Request.Builder()
                    .url("http://172.20.10.3:8080/fb-user")
                    //.addHeader("Authorization", "Bearer " + "token")
                    .post(body)
                    .build();


            getClient().newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.i("TAG", e.toString());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (!response.isSuccessful()) {
                        return;
                    }

                    String result = response.body().string();

                    //Collection<Profile> results = new Gson().fromJson(result, token);

                    String refreshedToken = FirebaseInstanceId.getInstance().getToken("897321265291", "FCM");

                    if(refreshedToken != null) {
                        ServerCallUtil.sendPushTokenToServer();
                    }
                }
            });
        }

    }


    private boolean isLoggedIn() {
        AccessToken accesstoken = AccessToken.getCurrentAccessToken();
        return !(accesstoken == null || accesstoken.getPermissions().isEmpty());
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onResume() {
        super.onResume();
        //redirect();
    }
}

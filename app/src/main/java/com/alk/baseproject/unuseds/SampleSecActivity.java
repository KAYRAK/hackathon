package com.alk.baseproject.unuseds;

import android.os.Bundle;
import android.util.Log;

import com.alk.baseproject.Constants;
import com.alk.baseproject.R;
import com.alk.baseproject.Util;
import com.alk.baseproject.base.BaseActivity;
import com.alk.baseproject.models.api.Account;
import com.alk.baseproject.models.api.DomesticTransferRequest;
import com.alk.baseproject.models.api.DomesticTransfers;
import com.alk.baseproject.models.api.Recipient;
import com.alk.baseproject.models.api.Sender;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SampleSecActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_sec);
        ButterKnife.bind(this);

        domesticTransfers();
    }


    private void accounts() {

        Request request = Util.createRequest("accounts/v1/banks/29000/accounts?customerId=1000001");

        getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("TAG", e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {
                    return;
                }

                String result = response.body().string();

                Log.i("TAG", result);

                ArrayList<Account> results = new Gson().fromJson(result, new TypeToken<ArrayList<Account>>() {
                }.getType());

                Log.i("TAG", results.get(0).getId());

            }
        });
    }


    private void domesticTransfers() {

        DomesticTransferRequest domesticTransferRequest = new DomesticTransferRequest();

        domesticTransferRequest.setAmount(1);
        domesticTransferRequest.setCause("");
        domesticTransferRequest.setCurrency("");
        domesticTransferRequest.setRequestedExecutionDate(0);

        Recipient recipient = new Recipient();
        recipient.setIban("SI99299117700000299");
        recipient.setName("");
        domesticTransferRequest.setRecipient(recipient);

        Sender sender = new Sender();
        sender.setIban("SI99299117700000199");
        sender.setName("");
        domesticTransferRequest.setSender(sender);

        RequestBody body = RequestBody.create(Constants.JSON, new Gson().toJson(domesticTransferRequest, DomesticTransferRequest.class));

        Request request = Util.createPostRequest("payments/v1/domesticTransfers?userId=1000001")
                .post(body)
                .build();

        getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("TAG", e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {
                    return;
                }

                String result = response.body().string();

                Log.i("TAG", result);

                DomesticTransfers domesticTransfers = new Gson().fromJson(result, DomesticTransfers.class);

                Log.i("TAG", domesticTransfers.toString());

            }
        });
    }
}

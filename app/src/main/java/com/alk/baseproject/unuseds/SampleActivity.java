package com.alk.baseproject.unuseds;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.alk.baseproject.Manifest;
import com.alk.baseproject.R;
import com.alk.baseproject.base.BaseActivity;
import com.alk.baseproject.activities.FacebookLoginActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class SampleActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        tb.setTitle("Example");
    }

    protected void btnClicked(View view) {

        startActivity(new Intent(this, FacebookLoginActivity.class));

        Request request = new Request.Builder()
                .url("https://us-central1-ace-ellipse-147212.cloudfunctions.net/getProfiles/auth?")
                .addHeader("Authorization", "Bearer " + "token")
                .build();


        getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {

                    return;
                }

                String result = response.body().string();

                //Collection<Profile> results = new Gson().fromJson(result, token);

            }
        });
    }

    @OnClick(R.id.btnAskPermission)
    protected void permissionAsked() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.RECORD_AUDIO
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {

                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "onPermissionsChecked", Snackbar.LENGTH_LONG);
                snackbar.show();

            }
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "onPermissionRationaleShouldBeShown", Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        }).check();

    }
}

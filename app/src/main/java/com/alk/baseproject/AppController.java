package com.alk.baseproject;

import android.app.Activity;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.alk.baseproject.models.server.Account;

import java.util.ArrayList;

import okhttp3.OkHttpClient;

public class AppController extends MultiDexApplication {

    public static ArrayList<Account> accounts;

    private static AppController mInstance;
    private OkHttpClient okHttpClient;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "onCreate: AppController");
        registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks());

        okHttpClient = new OkHttpClient();
        mInstance = this;

    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static final String TAG = "AppController";

    private static final class MyActivityLifecycleCallbacks implements ActivityLifecycleCallbacks {
        public void onActivityCreated(Activity activity, Bundle bundle) {
            Log.i(TAG, "onActivityCreated:" + activity.getLocalClassName());
        }
        public void onActivityDestroyed(Activity activity) {
            Log.i(TAG, "onActivityDestroyed:" + activity.getLocalClassName());
        }
        public void onActivityPaused(Activity activity) {
            Log.i(TAG, "onActivityPaused:" + activity.getLocalClassName());
        }
        public void onActivityResumed(Activity activity) {
            Log.i(TAG, "onActivityResumed:" + activity.getLocalClassName());
        }
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            Log.i(TAG, "onActivitySaveInstanceState:" + activity.getLocalClassName());
        }
        public void onActivityStarted(Activity activity) {
            Log.i(TAG, "onActivityStarted:" + activity.getLocalClassName());
        }
        public void onActivityStopped(Activity activity) {
            Log.i(TAG, "onActivityStopped:" + activity.getLocalClassName());
        }
    }
}

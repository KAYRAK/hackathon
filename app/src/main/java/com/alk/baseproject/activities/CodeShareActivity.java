package com.alk.baseproject.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.TextView;

import com.alk.baseproject.R;
import com.alk.baseproject.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CodeShareActivity extends BaseActivity {


    @BindView(R.id.txtCode)
    TextView txtCode;

    String code;



    @BindView(R.id.btnShare)
    TextView btnShare;

    @BindView(R.id.btnToDashboard)
    TextView btnToDashboard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_share);
        ButterKnife.bind(this);

        code = getIntent().getStringExtra("code");
        txtCode.setText(code);



        btnShare.getBackground().setColorFilter(0xFF228B22, PorterDuff.Mode.MULTIPLY);
        btnShare.setTextColor(Color.WHITE);

        btnToDashboard.getBackground().setColorFilter(0xFF419bf4, PorterDuff.Mode.MULTIPLY);
        btnToDashboard.setTextColor(Color.WHITE);

    }


    @OnClick(R.id.btnShare)
    public void btnShare() {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Here is your group code: " + code);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }


    @OnClick(R.id.btnToDashboard)
    public void btnToDashboard() {
        Intent intent = new Intent(CodeShareActivity.this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}

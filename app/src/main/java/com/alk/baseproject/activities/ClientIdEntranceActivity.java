package com.alk.baseproject.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.alk.baseproject.AppController;
import com.alk.baseproject.Constants;
import com.alk.baseproject.R;
import com.alk.baseproject.Util;
import com.alk.baseproject.base.BaseActivity;
import com.alk.baseproject.models.CreateGroupRequestModel;
import com.alk.baseproject.models.api.Account;
import com.facebook.Profile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ClientIdEntranceActivity extends BaseActivity {

    @BindView(R.id.banks_spinner)
    Spinner banksSpinner;

    @BindView(R.id.edtClientId)
    EditText edtClientId;

    @BindView(R.id.lstAccounts)
    ListView lstAccounts;

    @BindView(R.id.lyt2)
    LinearLayout lytBottomButtons;

    @BindView(R.id.btnGetAccounts)
    Button btnGetAccounts;

    @BindView(R.id.btnJoin)
    Button btnJoin;

    @BindView(R.id.btnCreate)
    Button btnCreate;

    ArrayAdapter<String> adapterX;

    ArrayList<String> names = new ArrayList<String>();
    ArrayList<Account> resultsForNames = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_id_entrance);

        ButterKnife.bind(this);


        btnGetAccounts.getBackground().setColorFilter(0xFF419bf4, PorterDuff.Mode.MULTIPLY);
        btnGetAccounts.setTextColor(Color.WHITE);

        btnJoin.getBackground().setColorFilter(0xFF419bf4, PorterDuff.Mode.MULTIPLY);
        btnJoin.setTextColor(Color.WHITE);

        btnCreate.getBackground().setColorFilter(0xFF419bf4, PorterDuff.Mode.MULTIPLY);
        btnCreate.setTextColor(Color.WHITE);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.banks_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        banksSpinner.setAdapter(adapter);


        banksSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                edtClientId.setText("");

                names.clear();
                resultsForNames.clear();

                adapterX.notifyDataSetChanged();

                if (position == 0) {

                } else {
                    Toast.makeText(ClientIdEntranceActivity.this, "Choose Unicredit For Hackathon.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterX = new ArrayAdapter<String>(ClientIdEntranceActivity.this, android.R.layout.simple_list_item_multiple_choice, names);

        lstAccounts.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lstAccounts.setAdapter(adapterX);
    }


    @OnClick(R.id.btnGetAccounts)
    public void btnClicked() {

        Request request = Util.createRequest("accounts/v1/banks/29000/accounts?customerId=" + edtClientId.getText().toString().trim());

        getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast.makeText(ClientIdEntranceActivity.this, "Something bad happened.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {

                    ClientIdEntranceActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(ClientIdEntranceActivity.this, "Something bad happened.", Toast.LENGTH_SHORT).show();
                        }
                    });

                    return;
                }

                String result = response.body().string();

                ArrayList<Account> results = new Gson().fromJson(result, new TypeToken<ArrayList<Account>>() {
                }.getType());

                names.clear();
                resultsForNames.clear();

                resultsForNames.addAll(results);

                for (Account acc : results) {
                    names.add(acc.getName() + " - " + acc.getArrangementIdentifier().getIban());
                }


                ClientIdEntranceActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        adapterX.notifyDataSetChanged();

                        lytBottomButtons.setVisibility(View.VISIBLE);

                        View view = ClientIdEntranceActivity.this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }
                });

            }
        });

    }


    @OnClick(R.id.btnCreate)
    public void btnCreate() {

        ArrayList<com.alk.baseproject.models.server.Account> accounts = new ArrayList<>();

        int len = lstAccounts.getCount();
        SparseBooleanArray checked = lstAccounts.getCheckedItemPositions();
        for (int i = 0; i < len; i++) {
            if (checked.get(i)) {

                //String item = names.get(i);

                Account acc = resultsForNames.get(i);

                com.alk.baseproject.models.server.Account account = new com.alk.baseproject.models.server.Account();
                account.setAccountid(acc.getId());
                //ALK is ok?? hardcoded!
                account.setBankid("29000");
                accounts.add(account);

            }
        }

        loadingDialog.show();

        CreateGroupRequestModel createGroupRequestModel = new CreateGroupRequestModel();
        createGroupRequestModel.setFacebookId(Profile.getCurrentProfile().getId());

        createGroupRequestModel.setAccounts(accounts);

        createGroupRequestModel.setGroupName("My Family");

        RequestBody body = RequestBody.create(Constants.JSON, new Gson().toJson(createGroupRequestModel, CreateGroupRequestModel.class));

        Request request = new Request.Builder()
                .url("http://"+Constants.IP+":8080/user/createGroup")
                .post(body)
                .build();


        getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {
                    return;
                }

                String code = response.body().string();

                Intent intent = new Intent(ClientIdEntranceActivity.this, CodeShareActivity.class);
                intent.putExtra("code", code);

                startActivity(intent);

            }
        });


    }


    @OnClick(R.id.btnJoin)
    public void btnJoin() {

        ArrayList<com.alk.baseproject.models.server.Account> accounts = new ArrayList<>();

        int len = lstAccounts.getCount();
        SparseBooleanArray checked = lstAccounts.getCheckedItemPositions();
        for (int i = 0; i < len; i++) {
            if (checked.get(i)) {

                //String item = names.get(i);

                Account acc = resultsForNames.get(i);

                com.alk.baseproject.models.server.Account account = new com.alk.baseproject.models.server.Account();
                account.setAccountid(acc.getId());
                //ALK is ok?? hardcoded!
                account.setBankid("29000");
                accounts.add(account);

            }
        }

        AppController.getInstance().accounts = new ArrayList<>();
        AppController.getInstance().accounts.addAll(accounts);

        Intent intent = new Intent(ClientIdEntranceActivity.this, CodeEntranceActivity.class);
        startActivity(intent);

    }

    @Override
    protected void onStop() {
        loadingDialog.hide();
        super.onStop();
    }
}

package com.alk.baseproject.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alk.baseproject.Constants;
import com.alk.baseproject.R;
import com.alk.baseproject.base.BaseActivity;
import com.alk.baseproject.models.AccountDataModel;
import com.alk.baseproject.models.DashboardAccount;
import com.alk.baseproject.models.DashboardListAdapter;
import com.facebook.Profile;
import com.facebook.login.widget.ProfilePictureView;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class DashboardActivity extends BaseActivity {

    private static DashboardListAdapter adapter;
    @BindView(R.id.lstAccounts)
    ListView lstAccounts;

    @BindView(R.id.txtTotal)
    TextView txtTotal;

    @BindView(R.id.txtName)
    TextView txtName;

    //private Drawer result;
    //private AccountHeader headerResult = null;
    ArrayList<AccountDataModel> dataModels = new ArrayList<>();
    private PieChart mChart;

    private ProfilePictureView profilePictureView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        //profilePictureView = (ProfilePictureView) findViewById(R.id.user_pic);
        //profilePictureView.setCropped(true);

        Profile profile = Profile.getCurrentProfile();

        String fbId = Profile.getCurrentProfile().getId();

        //profilePictureView.setProfileId(fbId);

        ImageView profilePic = (ImageView) findViewById(R.id.user_pic2);

        Picasso.with(this)
                .load("https://graph.facebook.com/v2.2/" + fbId + "/picture?height=120&type=normal") //extract as User instance method
                .transform(new CropCircleTransformation())
                .resize(120, 120)
                .into(profilePic);


        txtName.setText("Hello, " + String.format("%s %s", profile.getFirstName(), profile.getLastName()));

        /*

        for (int i = 0; i < 5; i++) {
            dataModels.add(new AccountDataModel("name" + i, new Double(i + 1)));
        }
*/
        adapter = new DashboardListAdapter(dataModels, getApplicationContext());

        lstAccounts.setAdapter(adapter);
        lstAccounts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //DataModel dataModel= dataModels.get(position);

            }
        });

        setChart();

        getAccounts();

    }

    private void setChart() {

        mChart = (PieChart) findViewById(R.id.chart1);
        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        //mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);
        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(true);

    }


    private void setData() {

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        for (AccountDataModel model : dataModels) {
            entries.add(new PieEntry(new Float(model.getBalance())));
        }

        PieDataSet dataSet = new PieDataSet(entries, "My Family");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);


        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            ;//colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            ;//colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            ;//colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            ;//colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(0f);
        data.setValueTextColor(Color.BLACK);

        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }


    private void getAccounts() {


        String fbId = Profile.getCurrentProfile().getId();


        Request request = new Request.Builder()
                .url("http://" + Constants.IP + ":8080/account/" + fbId)
                .build();


        getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {

                    return;
                }

                String result = response.body().string();


                ArrayList<DashboardAccount> results = new Gson().fromJson(result, new TypeToken<ArrayList<DashboardAccount>>() {
                }.getType());

                dataModels.clear();

                int colorCode = 0;
                String facebookId = "";

                Double totalBalance = 0d;

                for (DashboardAccount acc : results) {
                    AccountDataModel accDataModel = new AccountDataModel(acc.getName(), acc.getAvailableBalance());
                    accDataModel.setFacebookId(acc.getFacebookId());
                    accDataModel.setGroupCode(acc.getGroupCode());

                    if (facebookId.isEmpty()) {
                        accDataModel.setColorCode(colorCode);
                        facebookId = acc.getFacebookId();
                    } else if (facebookId.equals(acc.getFacebookId())) {
                        accDataModel.setColorCode(colorCode);
                    } else {
                        colorCode++;
                        accDataModel.setColorCode(colorCode);
                        facebookId = acc.getFacebookId();
                    }

                    dataModels.add(accDataModel);

                    totalBalance += acc.getAvailableBalance();
                }

                final Double finalTotalBalance = totalBalance;
                DashboardActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        adapter.notifyDataSetChanged();
                        setData();


                        txtTotal.setText(NumberFormat.getCurrencyInstance(new Locale("it", "IT"))
                                .format(finalTotalBalance));

                    }
                });

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        //setupDrawer();
    }
/*
    private void setupDrawer() {

        ProfileDrawerItem profile = new ProfileDrawerItem().withName("currentUser.getDisplayName()")
                //.withIcon(currentUser.getPhotoUrl())
                .withIdentifier(100);

        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withTranslucentStatusBar(true)
                .addProfiles(profile)
                .withSelectionListEnabledForSingleProfile(false)
                .withProfileImagesClickable(true)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        Intent intent = null;
                        Profile cProfile = AppController.getInstance().getCurrentData().getCurrentProfile();
                        if (cProfile != null && cProfile.type == 0) {
                            intent = new Intent(MainActivity.this, TenantProfileActivity.class);
                        } else if (cProfile != null && cProfile.type == 1) {
                            intent = new Intent(MainActivity.this, SeekerProfileActivity.class);
                        }

                        if (cProfile != null) {
                            //noinspection HardCodedStringLiteral
                            intent.putExtra("profile", cProfile);
                            MainActivity.this.startActivity(intent);
                        }

                        return true;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .build();

        //Create the drawer
        result = new DrawerBuilder()
                .withActivity(this)
                .withHasStableIds(true)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new SectionDrawerItem().withName(R.string.rumi_act_main_drawer_profile),
                        new PrimaryDrawerItem().withName(R.string.rumi_act_main_drawer_edit_profile).withIcon(R.drawable.ic_person_black_24dp).withIdentifier(1).withSelectable(false),
                        new PrimaryDrawerItem().withName(R.string.rumi_act_main_drawer_change_profile_type).withIcon(R.drawable.ic_swap_horiz_black_24dp).withIdentifier(2).withSelectable(false),
                        new PrimaryDrawerItem().withName(R.string.rumi_act_main_drawer_create_badge).withIcon(R.drawable.ic_web_black_24px).withIdentifier(3).withSelectable(false),
                        new SectionDrawerItem().withName(R.string.rumi_act_main_drawer_other),
                        new PrimaryDrawerItem().withName(R.string.rumi_act_main_drawer_share).withIcon(R.drawable.ic_share_black_24px).withIdentifier(22).withSelectable(false),
                        new PrimaryDrawerItem().withName(R.string.rumi_act_main_drawer_feedback).withIcon(R.drawable.ic_feedback_black_24px).withIdentifier(21).withSelectable(false),
                        new SecondaryDrawerItem().withName(R.string.rumi_act_main_drawer_exit).withIcon(R.drawable.ic_power_settings_new_black_24dp).withIdentifier(20).withSelectable(false)
                ) // add the items we want to use with our Drawer
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        if (drawerItem != null) {
                            Intent intent = null;
                            if (drawerItem.getIdentifier() == 2) {
                                intent = new Intent(MainActivity.this, ProfileTypeSelectionActivity.class);
                            } else if (drawerItem.getIdentifier() == 20) {
                                MainActivity.this.signOutFromApp();
                            } else if (drawerItem.getIdentifier() == 21) {
                                intent = new Intent(MainActivity.this, FeedbackActivity.class);
                            } else if (drawerItem.getIdentifier() == 22) {
                                Misc.shareApplication(MainActivity.this);
                            } else if (drawerItem.getIdentifier() == 1) {
                                Profile profile = AppController.getInstance().getCurrentData().getCurrentProfile();
                                if (profile == null) {
                                    intent = new Intent(MainActivity.this, ProfileTypeSelectionActivity.class);
                                } else if (profile.type == 0) {
                                    intent = new Intent(MainActivity.this, TenantActivity.class);
                                } else {
                                    intent = new Intent(MainActivity.this, SeekerActivity.class);
                                }
                            } else if (drawerItem.getIdentifier() == 3) {
                                Profile profile = AppController.getInstance().getCurrentData().getCurrentProfile();
                                if (profile == null) {
                                    intent = new Intent(MainActivity.this, ProfileTypeSelectionActivity.class);
                                } else {
                                    intent = new Intent(MainActivity.this, BadgeActivity.class);
                                    intent.putExtra("profile", profile);
                                }
                            }
                            if (intent != null) {
                                MainActivity.this.startActivity(intent);
                            }
                        }

                        return false;
                    }
                })
                .withShowDrawerOnFirstLaunch(true)
                .build();


        //only set the active selection or active profile if we do not recreate the activity
        // set the selection to the item with the identifier 11
        //result.setSelection(1, false);

        //set the active profile
        headerResult.setActiveProfile(profile);

    }
*/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}

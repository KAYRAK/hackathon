package com.alk.baseproject.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.alk.baseproject.AppController;
import com.alk.baseproject.Constants;
import com.alk.baseproject.R;
import com.alk.baseproject.base.BaseActivity;
import com.alk.baseproject.models.CreateGroupRequestModel;
import com.alk.baseproject.models.JoinGroupRequestModel;
import com.facebook.Profile;
import com.google.gson.Gson;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CodeEntranceActivity extends BaseActivity {


    @BindView(R.id.edtCode)
    EditText edtCode;

    @BindView(R.id.btnProceed)
    Button btnProceed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_entrance);
        ButterKnife.bind(this);

        btnProceed.getBackground().setColorFilter(0xFF419bf4, PorterDuff.Mode.MULTIPLY);
        btnProceed.setTextColor(Color.WHITE);
    }


    @OnClick(R.id.btnProceed)
    public void btnProceed() {

        loadingDialog.show();

        JoinGroupRequestModel joinGroupRequestModel = new JoinGroupRequestModel();
        joinGroupRequestModel.setFacebookId(Profile.getCurrentProfile().getId());

        joinGroupRequestModel.setAccounts(AppController.getInstance().accounts);

        joinGroupRequestModel.setGroupCode(edtCode.getText().toString().trim());

        RequestBody body = RequestBody.create(Constants.JSON, new Gson().toJson(joinGroupRequestModel, JoinGroupRequestModel.class));

        Request request = new Request.Builder()
                .url("http://"+Constants.IP+":8080/user/joinGroup")
                .post(body)
                .build();



        getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (!response.isSuccessful()) {
                    return;
                }

                String code = response.body().string();


                Intent intent = new Intent(CodeEntranceActivity.this, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);

            }
        });

    }

    @Override
    protected void onStop() {
        loadingDialog.hide();
        super.onStop();
    }
}

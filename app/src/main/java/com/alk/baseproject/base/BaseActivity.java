package com.alk.baseproject.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;

import com.alk.baseproject.AppController;
import com.alk.baseproject.LoadingDialog;
import com.alk.baseproject.R;

import okhttp3.OkHttpClient;

@SuppressWarnings("HardCodedStringLiteral")
public class BaseActivity extends AppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(this);

    }


    public OkHttpClient getClient() {
        return ((AppController) getApplication()).getOkHttpClient();
    }


}

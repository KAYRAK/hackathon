package com.alk.baseproject;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;

/**
 * Created by akayrak on 24/08/2017.
 */

public class LoadingDialog extends Dialog {

    public LoadingDialog(@NonNull Context context) {
        super(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        init();
    }

    public LoadingDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        init();
    }

    private void init() {
        setCancelable(false);
        setContentView(R.layout.dialog);
        getWindow().setBackgroundDrawableResource(R.color.dialogBackground);
    }
}

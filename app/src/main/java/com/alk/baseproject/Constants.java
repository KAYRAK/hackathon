package com.alk.baseproject;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;

/**
 * Created by akayrak on 02/09/2017.
 */

public class Constants {

    public static MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static List<Integer> colors = new ArrayList<Integer>() {{
        add(Color.BLUE);
        add(Color.RED);
        add(Color.GREEN);
    }};


    public static String IP = "192.168.132.179";
}
